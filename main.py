import requests


class ITunesAPI:
    def __init__(self, genre_id):
        self.genre_id = genre_id

    def fetch_genre_name(self):
        base_url = "https://itunes.apple.com/WebObjects/MZStoreServices.woa/ws/genres?id="
        url = base_url + str(self.genre_id)
        try:
            response = requests.get(url)
            response.raise_for_status()
            genre_info = response.json()
            genre_name = genre_info[str(self.genre_id)]['name']
            return genre_name
        except requests.exceptions.HTTPError as err:
            print("HTTP error occurred: ", err)
            return None
        except Exception as err:
            print("Something went wrong: ", err)
            return None

    def fetch_movie_streaming_platform(self):
        # Implement logic to fetch streaming platform for a movie
        # You can use APIs or a predefined list to determine the streaming platform
        movie_title = self._get_movie_title()  # Replace this with the actual method to get the movie title
        if movie_title == "comedy":
            return "Netflix"
        elif movie_title == "action":
            return "HBO"
        elif movie_title == "drama":
            return "Amazon Prime"
        else:
            return None

    def _get_movie_title(self):
        # Replace this with the actual method to get the movie title
        # This is a placeholder method for demonstration purposes
        return "comedy"


def main():
    while True:
        genre_id = input("Please enter a genre ID: ")
        if genre_id.isdigit():
            api = ITunesAPI(int(genre_id))
            genre_name = api.fetch_genre_name()
            if genre_name is not None:
                print(f"The genre for the ID {genre_id} is: {genre_name}")
                streaming_platform = api.fetch_movie_streaming_platform()
                if streaming_platform is not None:
                    print(f"The movie can be streamed on: {streaming_platform}")
                else:
                    print("Unable to fetch streaming platform.")
                break
            else:
                print("Unable to fetch genre. Please try again.")
        else:
            print("Invalid input. Please enter a valid genre ID.")


if __name__ == "__main__":
    main()
