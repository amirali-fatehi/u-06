# Use the official Python 3.11 image as the base
FROM python:3.11

# Set the working directory inside the container
WORKDIR /app

# Copy project files to the container
COPY . /app

# Install project dependencies
RUN pip install -r requirements.txt

# Specify the command to run when the container starts
CMD ["python", "main.py"]
