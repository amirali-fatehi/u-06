import main
import unittest


class TestITunesAPI(unittest.TestCase):
    def test_fetch_genre_name(self):
        api = main.ITunesAPI(1301)
        genre_name = api.fetch_genre_name()
        self.assertEqual(genre_name, 'Arts')  # Assuming the API correctly returns "Arts" for ID 1301

    def test_fetch_movie_streaming_platform(self):
        api = main.ITunesAPI(1301)
        streaming_platform = api.fetch_movie_streaming_platform()
        self.assertEqual(streaming_platform, 'Netflix')  # Assuming the movie with the genre ID 1301 can be streamed on Netflix


if __name__ == "__main__":
    unittest.main()
