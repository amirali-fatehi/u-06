This code will give you a random movie recommendation based on what genre you input.
You run this code in the terminal and it will ask you what genre you are interested in,
based on what you input it will give you the name of a movie within that genre.
If you input a genre that does not exsist or if you misspell the application will send an error and close itself. 

TODO: 
    * Add in so the app doesnt close when given error, let the user get one more try to write in a genre.
    * Add in so user can see where you can watch the movie, ex. Netflix, HBO.
    * Docker image.
